//todo, CLASE ABSTRACTAS

//! Las clases abstractas solo sirven como cascarones o plantillas de otras
abstract class Vehiculo {
  bool encendido = false;

  // Metodo opcional
  void encender() {
    encendido = true;
    print('Vehiculo encendido');
  }

  // Metodo opcional
  void apagar() {
    encendido = false;
    print('Vehiculo apagado');
  }

  // Metodo obligatorio para las clases que lo HEREDAN
  bool revisarMotor();
}

//* Extends, sirve para darte todas las propiedades y metodos que tiene la calse padre del que HEREDA
//! Tambien podemos heredarlo de una clase abstracta
class Carro extends Vehiculo {
  int kilometraje = 0;

  //! Creacion de Metodo obligatorio
  bool revisarMotor() {
    print('Revisando motor');
    return true;
  }
}

void main(List<String> args) {
  //! No se puede instanciar un objeto de una Clase Abstracta
  // final ford = new Vehiculo();
  final ford2 = new Carro();

  ford2.encender();
}
