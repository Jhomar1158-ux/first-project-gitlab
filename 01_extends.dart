class Vehiculo {
  bool encendido = false;

  void encender() {
    encendido = true;
    print('Vehiculo encendido');
  }

  void apagar() {
    encendido = false;
    print('Vehiculo apagado');
  }
}

//! Cada vez que reuses codigo puedes optimizarlo

//* Extends, sirve para darte todas las propiedades y metodos que tiene la calse padre del que HEREDA
class Carro extends Vehiculo {
  int kilometraje = 0;
}

void main(List<String> args) {
  final ford = new Carro();

  ford.encender();
}
